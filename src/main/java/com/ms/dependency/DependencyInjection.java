package com.ms.dependency;


import com.ms.test.TestA;

public class DependencyInjection {

    public TestA createInstanceOfTestA() {
        // TODO: implementation

        // use Java reflection
        Class<TestA> clazzTestA = TestA.class;

        // create new instance of B
        // inspect class B and find every field, which has annotation MyResource
        // for that field, you need to find class (which will be A)
        // you need to create an instance of A and set this instance into that field
        // return instance of B

        return null;
    }
}
