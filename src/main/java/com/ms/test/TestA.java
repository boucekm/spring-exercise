package com.ms.test;

import com.ms.dependency.Inject;

/**
 * Created by c2000497 on 12.11.15.
 */
public class TestA {

    @Inject
    private TestB testB;

    public String sayHello() {
        return testB.hello();
    }

    public TestB getTestB() {
        return testB;
    }
}
