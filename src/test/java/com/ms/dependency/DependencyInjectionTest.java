package com.ms.dependency;

import com.ms.test.TestA;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class DependencyInjectionTest {

    @Test
    public void testCreateInstanceOfTestA() throws Exception {
        TestA instanceOfTestA = new DependencyInjection().createInstanceOfTestA();

        assertThat(instanceOfTestA, notNullValue());
        assertThat(instanceOfTestA.getTestB(), notNullValue());
        assertThat(instanceOfTestA.sayHello(), is("Hello world!"));
    }
}
